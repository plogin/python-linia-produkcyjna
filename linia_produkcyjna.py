import sys
import json
from PySide2.QtWidgets import *
from PySide2.QtCore import *
import numpy as np
import gui.gui_linia as gui_linia


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = gui_linia.Ui_Linia()
        self.ui.setupUi(self)
        self.username_text = ''
        self.password_text = ''
        self.is_user_logged_in = False
        self.user_list = []
        self.load_user_file()
        self.motor_speed = np.zeros(3, dtype=np.int16)
        self.motor_max_speed = np.zeros(3, dtype=np.int16)
        self.motor_temperature = np.zeros(3, dtype=np.int16)
        self.motor_on = [True, True, True]
        self.motor_cooling = [False, False, False]
        # inicjalizacja elementów ui
        self.ui.login_btn.clicked.connect(self.user_login)
        self.ui.change_pass_btn.clicked.connect(self.change_password)
        self.ui.add_user_btn.clicked.connect(self.add_new_user)
        self.ui.username_textbox.textChanged.connect(self.username_changed)
        self.ui.password_textbox.textChanged.connect(self.password_changed)
        self.ui.password_textbox.setEchoMode(QLineEdit.Password)
        self.ui.motor_1_velocity_slider.valueChanged.connect(self.change_motor_speed)
        self.ui.motor_2_velocity_slider.valueChanged.connect(self.change_motor_speed)
        self.ui.motor_3_velocity_slider.valueChanged.connect(self.change_motor_speed)
        # message box obecnosc
        self.msg_box = QMessageBox()
        self.msg_box.setStandardButtons(QMessageBox.Ok)
        self.msg_box.setIcon(QMessageBox.Information)
        self.msg_box.setWindowTitle('Kontrola obecności')
        self.msg_box.setText('Proszę potwierdzić swoją obecność przez naciśnięcie przycisku Ok')
        # message box awaria
        self.msg_box1 = QMessageBox()
        self.msg_box1.setStandardButtons(QMessageBox.Ok)
        self.msg_box1.setIcon(QMessageBox.Warning)
        self.msg_box1.setWindowTitle('Awaria silnika')
        self.msg_box1.setText('Awaria')
        # Timer
        self.presence_timer = QTimer()
        self.step_timer = QTimer()
        self.step_timer.timeout.connect(self.change_state)
        self.user_login_show_elements()
        self.step_timer.start(500)

    def change_state(self):
        for i in range(len(self.motor_speed)):
            # Zabezpieczenie po wylogowaniu użytkownika
            if self.is_user_logged_in is False:
                self.motor_on[i] = False
                self.motor_max_speed[i] = 0
            # Zmiana predkosci silnika
            if self.motor_on[i] is True:
                if self.motor_speed[i] < self.motor_max_speed[i]:
                    self.motor_speed[i] += 5
                    if self.motor_speed[i] > self.motor_max_speed[i]:
                        self.motor_speed[i] = self.motor_max_speed[i]
                elif self.motor_speed[i] > self.motor_max_speed[i]:
                    self.motor_speed[i] -= 10
                    if self.motor_speed[i] < self.motor_max_speed[i]:
                        self.motor_speed[i] = self.motor_max_speed[i]
            elif self.motor_on[i] is False and self.motor_speed[i] > 0:
                self.motor_speed[i] -= 10
                if self.motor_speed[i] < 0:
                    self.motor_speed[i] = 0
            # Zmiana temperatury silnika
            self.motor_temperature[i] += int(self.motor_speed[i] / 10 * np.random.randint(5)) - 1
            if self.motor_cooling[i] is True:
                self.motor_temperature[i] -= 5
            if self.motor_temperature[i] < 0:
                self.motor_temperature[i] = 0
            # Awarie związane z temperaturą
            if self.motor_temperature[i] > 100:
                self.motor_on[i] = False
            elif self.motor_temperature[i] > 70:
                pass
            elif self.motor_cooling[i] is False and self.motor_temperature[i] > 40:
                self.motor_cooling[i] = True
            elif self.motor_cooling[i] is True and self.motor_temperature[i] < 11:
                self.motor_cooling[i] = False
                self.motor_on[i] = True
            # Miejsce na losowe awarie
            if self.motor_on[i] is True and self.motor_speed[i] > 5 and np.random.randint(100) > 98:
                    self.msg_box1.setWindowTitle('Awaria silnika {}'.format(i + 1))
                    self.msg_box1.setText('Losowa awaria silnika {}. Wcisnij ok aby naprawić.'.format(i + 1))
                    self.msg_box1.exec()
        self.change_motor_text()

    def change_motor_text(self):
        self.ui.motor_1_velocity_line_edit.setText(str(self.motor_speed[0]))
        self.ui.motor_2_velocity_line_edit.setText(str(self.motor_speed[1]))
        self.ui.motor_3_velocity_line_edit.setText(str(self.motor_speed[2]))
        self.ui.motor_1_temperature_line_edit.setText(str(self.motor_temperature[0]))
        self.ui.motor_2_temperature_line_edit.setText(str(self.motor_temperature[1]))
        self.ui.motor_3_temperature_line_edit.setText(str(self.motor_temperature[2]))
        if self.motor_cooling[0] is True:
            self.ui.motor_1_cooling_line_edit.setText('Włączone')
            self.ui.motor_1_cooling_line_edit.setStyleSheet("color: black; background-color: rgb(0, 255, 0);")
        else:
            self.ui.motor_1_cooling_line_edit.setText('Wyłączone')
            self.ui.motor_1_cooling_line_edit.setStyleSheet("color: black; background-color: rgb(255, 0, 0);")
        if self.motor_cooling[1] is True:
            self.ui.motor_2_cooling_line_edit.setText('Włączone')
            self.ui.motor_2_cooling_line_edit.setStyleSheet("color: black; background-color: rgb(0, 255, 0);")
        else:
            self.ui.motor_2_cooling_line_edit.setText('Wyłączone')
            self.ui.motor_2_cooling_line_edit.setStyleSheet("color: black; background-color: rgb(255, 0, 0);")
        if self.motor_cooling[2] is True:
            self.ui.motor_3_cooling_line_edit.setText('Włączone')
            self.ui.motor_3_cooling_line_edit.setStyleSheet("color: black; background-color: rgb(0, 255, 0);")
        else:
            self.ui.motor_3_cooling_line_edit.setText('Wyłączone')
            self.ui.motor_3_cooling_line_edit.setStyleSheet("color: black; background-color: rgb(255, 0, 0);")
        self.change_motor_speed()

    # Funkcje kontrolek gui
    def change_motor_speed(self):
        if self.is_user_logged_in is True:
            self.motor_max_speed[0] = self.ui.motor_1_velocity_slider.value()
            self.motor_max_speed[1] = self.ui.motor_2_velocity_slider.value()
            self.motor_max_speed[2] = self.ui.motor_3_velocity_slider.value()
        else:
            self.ui.motor_1_velocity_slider.setValue(0)
            self.ui.motor_2_velocity_slider.setValue(0)
            self.ui.motor_3_velocity_slider.setValue(0)

    def user_login(self):
        if self.is_user_logged_in is True:
            self.presence_timer.stop()
            self.is_user_logged_in = False
            self.ui.username_textbox.setReadOnly(False)
            self.ui.password_textbox.setReadOnly(False)
            self.password_text = ''
            self.ui.username_textbox.setText(self.username_text)
            self.ui.password_textbox.setText(self.password_text)
            self.motor_max_speed = np.zeros(3, dtype=np.int16)
            self.change_motor_speed()
            self.motor_on = [False, False, False]
            self.ui.login_btn.setText('Zaloguj')
        else:
            try:
                self.user_list.index([self.username_text, self.password_text])
                self.is_user_logged_in = True
                self.ui.username_textbox.setReadOnly(True)
                self.ui.password_textbox.setReadOnly(True)
                self.ui.login_btn.setText('Wyloguj')
                self.change_motor_speed()
                self.motor_on = [True, True, True]
                self.presence_timer = QTimer()
                self.presence_timer.timeout.connect(self.check_user_presence)
                self.presence_timer.setSingleShot(True)
                self.presence_timer.start(30000)
            except ValueError:
                pass
        self.user_login_show_elements()

    def user_login_show_elements(self):
        opt = False
        if self.is_user_logged_in is True:
            opt = True
        self.ui.add_user_gBox.setVisible(opt)
        self.ui.change_pass_gBox.setVisible(opt)
        self.ui.new_pass_gBox.setVisible(opt)
        self.ui.new_user_name_gBox.setVisible(opt)
        self.ui.new_user_pass_gBox.setVisible(opt)
        self.ui.new_pass_textbox.setVisible(opt)
        self.ui.new_user_name_textbox.setVisible(opt)
        self.ui.new_user_pass_textbox.setVisible(opt)

    def username_changed(self):
        self.username_text = self.ui.username_textbox.text()

    def password_changed(self):
        self.password_text = self.ui.password_textbox.text()

    def change_password(self):
        password = self.ui.new_pass_textbox.text()
        u_id = self.user_list.index([self.username_text, self.password_text])
        self.user_list[u_id][1] = password
        self.password_text = password
        self.ui.password_textbox.setText(self.password_text)
        self.update_user_file()
        self.ui.new_pass_textbox.setText('')

    def add_new_user(self):
        username = self.ui.new_user_name_textbox.text()
        password = self.ui.new_user_pass_textbox.text()
        if username != '':
            self.user_list.append([username, password])
            self.update_user_file()
            self.ui.new_user_name_textbox.setText('')
            self.ui.new_user_pass_textbox.setText('')

    # Funkcje kontroli obecności
    def check_user_presence(self):
        self.presence_timer.timeout.disconnect()
        self.presence_timer.timeout.connect(self.user_not_present)
        self.presence_timer.setSingleShot(True)
        self.presence_timer.start(30000)
        self.msg_box.exec()
        self.presence_timer.stop()
        self.presence_timer.timeout.disconnect()
        self.presence_timer.timeout.connect(self.check_user_presence)
        self.presence_timer.setSingleShot(True)
        self.presence_timer.start(30000)

    def user_not_present(self):
        self.msg_box.close()
        self.user_login()

    # Obsługa pliku z uzytkownikami
    def load_user_file(self):
        try:
            with open('users/users.json') as f:
                user_dict = json.load(f)
                for elem in user_dict.items():
                    user = [elem[1][0], elem[1][1]]
                    self.user_list.append(user)
                f.close()
        except (FileNotFoundError, KeyError, ValueError):
            pass

    def update_user_file(self):
        user_dict = {}
        u_id = 0
        for user in self.user_list:
            user_elem = {}
            user_elem['id'] = user[0]
            user_elem['username'] = user[0]
            user_elem['password'] = user[1]
            user_dict['user{}'.format(u_id)] = user
            u_id += 1
        with open('users/users.json', 'w+') as f:
            json.dump(user_dict, f)
            f.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())
